package buzon;

import java.util.ArrayList;
import javax.swing.JOptionPane;


public class Correo {
    
    private static int totalCorreos =0; 
    /**
     * @return the totalCorreos
     */
    public static int getTotalCorreos() {
        return totalCorreos;
    }
    /**
     * @param aTotalCorreos the totalCorreos to set
     */
    public static void setTotalCorreos(int aTotalCorreos) {
        totalCorreos = aTotalCorreos;
    }
    /*
     * @autor Marina
     */
    private String texto;
    private int lido; // 1-> lido ; 0 --> non lido
    public Correo(){
        
    }
    public Correo(String te){
        texto = te;
        lido=0 ;
        totalCorreos ++ ;
    }
    /**
     * @return the texto
     * @autor Sara
     */
    public String getTexto() {
        return texto;
    }
    /**
     * @param texto the texto to set
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }
    /**
     * @return the lido
     */
    public int getLido() {
        return lido;
    }
    /**
     * @param lido the lido to set
     */
    public void setLido(int lido) {
        this.lido = lido;
    }
    
    public void engade(ArrayList<Correo> correos){
        String mensaxe = JOptionPane.showInputDialog("escribe mensaxe:");
        correos.add(new Correo(mensaxe));
        
    }
    /*
     * @autor marina 
     * modifico este metodo para que use el array
     * este metodo cuenta el numero de correos que hay en la lista   
     */
   public int numeroCorreos(ArrayList <Correo> email){
       return email.size();
   }
   public boolean porLer(ArrayList <Correo>correos){
       boolean leidos=true;// estan todos leidos
       for(Correo co :correos){
           if(co.getLido()== 1)
               leidos =false;
       }
       return leidos;       
   }
   /**
     * @autor Sara
     */
   public String amosaPrimerNonLeido(ArrayList<Correo>correos){
     String res="";
       for(Correo co:correos){
         if(co.getLido()== 0){
             res = co.getTexto();
             break;
         }
             
     }  
       return res;   
              
   }
    /**
     * @autor Sara
     * @autor Marina 
     * cambio la estructura para no necesitar int k
     */
   public void eliminar(ArrayList <Correo>correos){
        String res= JOptionPane.showInputDialog("introduce mensaje a buscar");
        int resp= Integer.parseInt(res);
        correos.remove(resp-1);
        System.out.println("mensaje"+resp+"eliminado");
   }
   public void amosa(int k,ArrayList<Correo>correos){
       for(int i =0;i<correos.size();i++)
           if(i== k){
               System.out.println("correo :."+ correos.get(i-1).getTexto());
               correos.get(i-1).setLido(1);      
       
   }
   
   } 
   
    public void verTodos(ArrayList<Correo>correos){
       for(Correo co:correos){
       System.out.println(" mensaxe "+ co.getTexto()+ " lido :" + co.getLido());
       
       }    
}
   
    
}
